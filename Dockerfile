FROM alpine
ADD script.sh /bin/
RUN chmod +x /bin/script.sh
RUN apk -Uuv add git curl ca-certificates jq
ENTRYPOINT /bin/script.sh
