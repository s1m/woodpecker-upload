#!/bin/sh

if [ -z "$PLUGIN_TOKEN" ]; then
  echo "ERR: token variable is empty"
  exit 1
fi

if [ -z "$PLUGIN_FILE" ]; then
  echo "ERR: file variable is empty"
  exit 1
fi

if [ -z "$PLUGIN_PACKAGE" ] && [ -z "$CI_COMMIT_TAG" ]; then
    echo "ERR: not a tag and CI is not configured to upload a generic package"
    exit 1
fi

if [ -z "$PLUGIN_VERSION" ]; then
    if [ -n "$CI_COMMIT_TAG" ]; then
        PLUGIN_VERSION="$CI_COMMIT_TAG"
    else
        PLUGIN_VERSION=$(echo "$CI_COMMIT_SHA" | cut -c-10)
    fi
fi

BASE_URL=$(echo "$CI_REPO_URL" | grep -Eo '^http[s]?://[^/]+')

if [ -n "$PLUGIN_PACKAGE" ]; then
    PACKAGE_SUBPATH="packages/${CI_REPO_OWNER}/generic/${CI_REPO_NAME}/${PLUGIN_VERSION}"
    PACKAGE_URL="${BASE_URL}/api/$PACKAGE_SUBPATH"
    PACKAGE_API_URL="${BASE_URL}/api/v1/$PACKAGE_SUBPATH"

    curl \
      -H "Authorization: token ${PLUGIN_TOKEN}" \
      -X 'DELETE' \
      --silent --output /dev/null \
      "$PACKAGE_API_URL"

    echo "$PLUGIN_FILE" | tr ',' '\n' | while read f; do
      BASE_FILENAME=$(basename "$f")
      echo "INFO: uploading $BASE_FILENAME to $BASE_URL"
      status_code=$(curl --location -H "Authorization: token ${PLUGIN_TOKEN}" --upload-file $f --write-out %{http_code} --silent --output /dev/null "$PACKAGE_URL/$BASE_FILENAME")

      if [ $status_code -ne 201 ]; then
        echo "ERR: failed to upload file ($status_code)"
        exit 1
      else
        echo "INFO: upload of $BASE_FILENAME successful to $PACKAGE_URL/${BASE_FILENAME}"
      fi
      echo "PACKAGE VERSION: ${BASE_URL}/${CI_REPO_OWNER}/-/packages/generic/${CI_REPO_NAME}/${PLUGIN_VERSION}"
    done
fi

if [ -n "$CI_COMMIT_TAG" ]; then
    RELEASE_API_URL="$BASE_URL/api/v1/repos/$CI_REPO_OWNER/$CI_REPO_NAME/releases"
    if [ -n "$PLUGIN_FASTLANE" ]; then
        RELEASE_BODY=$(cat "fastlane/metadata/android/en-US/changelogs/$(ls fastlane/metadata/android/en-US/changelogs/ | sort -g | tail -n1)")
    fi

    BODY=$(jq --null-input --arg tag "$CI_COMMIT_TAG" --arg name "$CI_COMMIT_TAG" --arg body "$RELEASE_BODY" '{"tag_name": $tag, "name": $name, "body": $body}')

    curl \
      --silent --output /dev/null \
      --location \
      -X POST \
      -H "Authorization: token ${PLUGIN_TOKEN}" \
      -H "Content-Type: application/json" \
      --data "$BODY" \
      "$RELEASE_API_URL"

    RELEASE_ID=$(curl -H "Authorization: token ${PLUGIN_TOKEN}" "$RELEASE_API_URL/tags/$CI_COMMIT_TAG" | jq -r ".id")

    echo "$PLUGIN_FILE" | tr ',' '\n' | while read f; do
      BASE_FILENAME=$(basename "$f")
      echo "INFO: uploading $BASE_FILENAME to $BASE_URL"
      status_code=$(curl -X POST -H "Authorization: token ${PLUGIN_TOKEN}" -F "attachment=@$f" --write-out %{http_code} --silent --output /dev/null "$RELEASE_API_URL/$RELEASE_ID/assets")

      if [ $status_code -ne 201 ]; then
        echo "ERR: failed to upload file ($status_code)"
        exit 1
      else
        echo "INFO: $BASE_FILENAME uploaded"
      fi
    done

fi

exit 0
