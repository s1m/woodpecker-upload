# woodpecker-upload

---

A Drone plugin for uploading files as generic package to Gitea package registry. 


### Example

```yaml
pipeline:
  build:
    image: alpine
    commands:
      - touch example.md
      
  artifacts:
    image: codeberg.org/s1m/woodpecker-upload
    settings:
      token:
        from_secret: codeberg_token
      file: ./example.md
      version: dev
      fastlane: true
      package: true
```

### Development

To test:

```console
$ PLUGIN_TOKEN=token PLUGIN_FILE=up1,up2 CI_COMMIT_SHA=test PLUGIN_PACKAGE
=true CI_REPO_OWNER=s1m CI_REPO_NAME=android-example CI_REPO_URL=https://codeberg.org/s1m/android-example ./script.sh
```
